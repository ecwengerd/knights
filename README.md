# Knights

A chess variant made with Godot 4.3.

<img src="art/starting_board.png" width="500" /><br />

All starting pieces are knights. A knight is promoted to a queen if it reaches the other side of the board.

Players may win by: <br />
&nbsp;&nbsp;&nbsp;&nbsp;1) Capturing all opponent pieces, OR <br />
&nbsp;&nbsp;&nbsp;&nbsp;2) Capturing all opponent knights, AND controlling more queens than the opponent.

## How to Play

Playable on Mac (tested on Monterey 12 and Sonoma 14) and Windows 10/11.

### From executable

Download the executable from the latest stable version corresponding to your OS (Mac or Windows) directly from [here](https://gitlab.com/ecwengerd/knights/-/releases).

### From source

Download [Godot 4.3](https://godotengine.org/download/archive/4.3-stable/) and the source code from the [latest release](https://gitlab.com/ecwengerd/knights/-/releases).
Open Godot, click Import, and choose this project. Once loaded, click the play icon near the top-right corner.
