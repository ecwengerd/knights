extends Node2D

var cursor_out_of_bounds: bool = false
var piece_animating: bool = false
var selected_piece: Piece = null

@onready var board = $Board
@onready var sound_player = %SoundPlayer


func _ready():
	
	Globals.game_started.connect(_on_game_started)
	Globals.piece_animation_started.connect(_on_piece_animation_started)
	Globals.piece_animation_stopped.connect(_on_piece_animation_stopped)
	Globals.square_selected.connect(_on_square_selected)

	board.mouse_entered.connect(_on_board_mouse_entered)
	board.mouse_exited.connect(_on_board_mouse_exited)


func _process(delta):
	
	if selected_piece != null:
		selected_piece.global_position = _update_piece_position(
			selected_piece,
			delta,
		)


func _on_game_started():
	board.setup()


func _on_piece_animation_started():
	piece_animating = true
	
	
func _on_piece_animation_stopped():
	piece_animating = false


func _on_board_mouse_entered():
	cursor_out_of_bounds = false

	
func _on_board_mouse_exited():
	cursor_out_of_bounds = true


func _update_piece_position(piece: Piece, delta):
	
	# Layer this piece above all other squares and pieces on the board.
	piece.set_z_index(2)
	
	# Position the center of the piece texture on the cursor.
	var mouse_middle_position = Vector2(
		piece.texture.get_width() / 2.0,
		piece.texture.get_height() / 2.0,
	)
	
	# Account for the offset of the board from the top left corner
	# of the game scene, since the board cannot be anchored to a
	# different location.
	var board_offset = (
		Vector2(board.offset_left, board.offset_top) if board.flipped
		else Vector2()
	)
	# Add additional offset to account for global_position rotating
	# with the piece texture.
	# See https://github.com/godotengine/godot/issues/95798
	var rotation_offset = -piece.size if board.flipped else Vector2()
	
	# Lerp the piece position with high velocity so it appears 
	# both smooth and snappy.
	var lerp_velocity = 60 * delta
	var new_position = lerp(
		piece.global_position,
		get_global_mouse_position()
			- mouse_middle_position
			+ board_offset
			+ rotation_offset
		,
		lerp_velocity,
	)
	
	return new_position


func get_piece_on_square(square: Square):
	
	# Get child nodes of the square, check that only one
	# child is a piece, and return that piece.
	var child_nodes = square.get_children()
	if not child_nodes:
		return null
	else:
		for child_node in child_nodes:
			if not child_node.is_in_group("pieces"):
				child_nodes.erase(child_node)
		
		var piece = child_nodes[0]
		return piece


func get_available_squares(piece: Piece) -> Array:

	var available_squares = []
	var coords = Globals.convert_notation_to_coordinates(piece.square)
	
	if piece is Knight:
		
		var possible_deltas = [[-2, -1], [-2, 1], [-1, -2], [-1, 2], [1, -2], [1, 2], [2, -1], [2, 1]]
		for possible_delta in possible_deltas:

			var possible_x = coords.x + possible_delta[0]
			if possible_x < 1 or possible_x > 8:
				continue
			
			var possible_y = coords.y + possible_delta[1]
			if possible_y < 1 or possible_y > 8:
				continue
			
			var next_square = Globals.convert_coordinates_to_notation(
				{"x": possible_x, "y": possible_y}
			)
			next_square = board.find_child(next_square)
		
			# Skip if a piece of the same color exists on the next square.
			var piece_on_next_square = get_piece_on_square(next_square)
			if piece_on_next_square != null:
				if piece_on_next_square.is_in_group(piece.color):
					continue
			
			available_squares.append(next_square)

	elif piece is Queen:
		
		var possible_deltas = [[0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]]
		for possible_delta in possible_deltas:
		
			var path_coords = coords.duplicate()
			var path_obstructed = false
			while not path_obstructed:
				
				# Get possible xy and check if out of bounds.
				path_coords.x += possible_delta[0]
				if path_coords.x < 1 or path_coords.x > 8:
					break
				path_coords.y += possible_delta[1]
				if path_coords.y < 1 or path_coords.y > 8:
					break
				
				var next_square = Globals.convert_coordinates_to_notation(
					{"x": path_coords.x, "y": path_coords.y}
				)
				next_square = board.find_child(next_square)
				
				var piece_on_next_square = get_piece_on_square(next_square)
				if piece_on_next_square == null:
					pass  # Continue the loop
					
				# Stop if a piece of the same color exists on the next square.
				elif piece_on_next_square.is_in_group(piece.color):
					break
				# If a piece of the opposite color exists on the next square,
				# add it to available squares, then break from this loop.
				elif piece_on_next_square.is_in_group(piece.opposite_color):
					path_obstructed = true
				
				available_squares.append(next_square)

	return available_squares


# highlight and unhighlight should ideally be combined into one
# function, but the lack of named arguments in GDScript functions
# makes un/highlighting ambiguous when called.
func _highlight_available_moves(piece: Piece) -> void:
	
	var available_squares = get_available_squares(piece)
	for square in available_squares:
		square.highlight()


func _unhighlight_available_moves(piece: Piece) -> void:
	
	var available_squares = get_available_squares(piece)
	for square in available_squares:
		square.unhighlight()


func _is_legal_move(piece: Piece, to_square: Square, from_square: Square) -> bool:
	
	if to_square.coords == from_square.coords:
		return false
	
	var available_squares = get_available_squares(piece)
	if to_square not in available_squares:
		return false
	
	var piece_on_square = get_piece_on_square(to_square)
	if piece_on_square != null:
		if piece_on_square.is_in_group(piece.color):
			return false
		
	return true


func _can_capture(capturer_piece: Piece, square: Square) -> bool:
	var piece_to_be_captured: Piece = get_piece_on_square(square)
	if piece_to_be_captured != null:
		if piece_to_be_captured.is_in_group(capturer_piece.opposite_color):
			return true
	
	return false


func _capture_piece(square: Square) -> void:
	
	var piece_to_be_captured: Piece = get_piece_on_square(square)
	piece_to_be_captured.remove_from_group(piece_to_be_captured.color)
	piece_to_be_captured.queue_free()
	
	_unhighlight_available_moves(piece_to_be_captured)
	Globals.piece_captured.emit(piece_to_be_captured)
	
	print("Captured piece ", piece_to_be_captured)
	
	
func _can_promote(piece: Piece) -> bool:
	
	if piece is Knight:
		var is_on_final_rank: bool = (
			(
				piece.square.coords.y == 1
				and piece.color == "black"
			)
			or
			(
				piece.square.coords.y == 8
				and piece.color == "white"
			)
		)
		if is_on_final_rank:
			return true
		
	return false


func play_move(piece: Piece, to_square: Square) -> void:
	
	await piece.move(to_square)
	var action = "move"
	
	if _can_capture(piece, to_square):
		_capture_piece(to_square)
		action = "capture"
	
	if _can_promote(piece):
		piece.promote("queen")
		action = "promote"

	sound_player.play_sound(action)
	
	Globals.turn_ended.emit(piece)


func _on_square_selected(pressed_or_released: String = "pressed"):
	
	# Lock all moves if the game has ended.
	if not Globals.game_in_progress:
		return
	
	# Lock all moves while another piece is animating.
	if piece_animating:
		return
	
	var to_square: Square = Globals.to_square
	var from_square: Square = Globals.from_square
	
	if pressed_or_released == "pressed":
		selected_piece = get_piece_on_square(from_square)
		if selected_piece != null:
			if selected_piece.is_in_group(Globals.human_color):
				_highlight_available_moves(selected_piece)
			else:
				selected_piece = null
		# _process() then detects if selected_piece != null during
		# each frame, and attaches the selected piece to the player's
		# cursor.
		
	elif pressed_or_released == "released":
		if selected_piece != null:
			_unhighlight_available_moves(selected_piece)
			if (
				not cursor_out_of_bounds
				and _is_legal_move(selected_piece, to_square, from_square)
				and selected_piece.color == Globals.color_turn
			):
				await play_move(selected_piece, to_square)
				
			else:
				print("Move to ", to_square, " from ", from_square, " was not played")
				# Snap the piece back to its original square.
				selected_piece.set_global_position(from_square.get_global_xform_position())
			
			# Reset the piece's layer position and remove it from selection.
			selected_piece.set_z_index(0)
			selected_piece = null
