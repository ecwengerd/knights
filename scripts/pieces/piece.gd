class_name Piece extends TextureRect

var color: String
var opposite_color: String

@onready var square: Square = self.get_parent()


func _init():
	# Set mouse filter to "ignore". We want all click-and-drag events
	# to occur in Squares on the Board, and other mouse filters cause
	# Piece objects to overlap with Squares above the Piece on the tree.
	# This causes mouse_entered() events to occur EVEN IF the Piece is
	# layered above the Square.
	self.mouse_filter = MOUSE_FILTER_IGNORE
	
	# Add to "pieces" group, because Godot can't seem to detect if
	# each specific scene is part of the Piece class.
	self.add_to_group("pieces")


func _ready():
	if self.is_in_group("white"):
		color = "white"
		opposite_color = "black"
	else:
		color = "black"
		opposite_color = "white"


func animate(
	from_position: Vector2,
	to_position: Vector2,
	transition=Tween.TRANS_LINEAR,
	duration=0.25,
):
	# Setting top level will allow the piece to inherit transform properties,
	# including rotation, from its parent(s) during animation.
	self.top_level = true
	self.show_behind_parent = false
	
	var offset = Vector2()
	
	# --- Begin animation ---
	var tween = create_tween()
	Globals.piece_animation_started.emit()
	
	var board_flipped = Globals.human_color == "black"
	if board_flipped:
		self.rotation_degrees += 180
		#offset = Vector2(-100, -100)
		
	tween.tween_property(self, "global_position", to_position + offset, duration)\
		.from(from_position + offset)\
		.set_trans(transition)
	
	await tween.finished
	if board_flipped:
		self.rotation_degrees += 180
	
	Globals.piece_animation_stopped.emit()
	# --- End animation ---
	
	self.top_level = false
	self.set_z_index(0)


func move(to_square):
	# Add the piece as a new child of the current square,
	# then remove its copy from the old square.
	# This assumes each Square contains no more than one Piece child.
	var from_square = self.square
	print("to_square: ", to_square)
	print("from_square: ", from_square)
	from_square.remove_child(self)
	to_square.add_child(self)
	
	if Globals.player_turn == "bot":
		self.top_level = true
		await self.animate(from_square.get_global_xform_position(), to_square.get_global_xform_position())
		self.top_level = false
		
	self.set_global_position(to_square.get_global_xform_position())

	self.square = to_square


func promote(new_piece="queen") -> void:

	var base_path: String = "res://scenes/pieces/{color}_{new_piece}.tscn"
	var new_piece_scene_path: String = base_path.format(
		{"color": color, "new_piece": new_piece}
	)
	var new_piece_scene = load(new_piece_scene_path)
	new_piece_scene = new_piece_scene.instantiate()
	
	new_piece_scene.add_to_group("pieces")
	new_piece_scene.add_to_group(color)
	
	var board_flipped = Globals.human_color == "black"
	if board_flipped:
		new_piece_scene.rotation_degrees += 180
	
	add_sibling(new_piece_scene)
	print("Promoted piece ", self)
	
	# Remove piece from color group so that game state checks do not 
	# detect the piece if queue_free() does not remove it quickly
	self.remove_from_group(color)
	queue_free()
