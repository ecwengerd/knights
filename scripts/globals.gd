extends Node

var human_color: String = "white"
var bot_color: String = "black"

var color_turn = "white"
var player_turn = "human"

var game_in_progress = false

var from_square: ColorRect
var to_square: ColorRect

const alpha_to_num: Dictionary = {
	"a": 1,
	"b": 2,
	"c": 3,
	"d": 4,
	"e": 5,
	"f": 6,
	"g": 7,
	"h": 8,
}
const num_to_alpha: Array = [-1, "a", "b", "c", "d", "e", "f", "g", "h"]

signal game_lost
signal game_started
signal game_won
signal main_menu

signal piece_animation_started
signal piece_animation_stopped

signal piece_captured(piece: Piece)
signal square_selected(is_selected: bool)

signal turn_ended(piece, to_square)


func convert_coordinates_to_notation(coords: Dictionary):
	var file: String = num_to_alpha[coords.x]
	var rank: String = str(coords.y)
	
	return file + rank
	
func convert_notation_to_coordinates(square):
	var x = str(square)[0]
	x = alpha_to_num[x]
	var y = int(str(square)[1])
	
	return {"x": x, "y": y}
