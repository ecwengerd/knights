extends Node

@export var muted = true

@onready var music_bank: Dictionary = {
	background = %BackgroundMusicPlayer,
	main_menu  = %MenuMusicPlayer,
}
@onready var music_playing = "background"

func _ready():
	
	Globals.game_started.connect(_on_game_started)
	Globals.main_menu.connect(_on_main_menu)
	
	if not muted:
		play(music_playing)


func _on_music_button_toggled(toggled_on):
	muted = false if toggled_on else true
	if not muted:
		play(music_playing)
	else:
		stop(music_playing)


func play(music: String, from_position: float = 0.0):
	
	music_bank[music_playing].stop()
	music_playing = music
	if not muted:
		music_bank[music].play(from_position)

func stop(music: String):
	music_bank[music].stop()


func _on_main_menu() -> void:
	# Use "background" for consistent BG music;
	# use "main_menu" for separate main menu and gameplay music
	if music_playing != "background":
		play("background")


func _on_game_started() -> void:
	if music_playing != "background":
		play("background")


func _on_background_music_player_finished():
	await get_tree().create_timer(0.5).timeout
	play("background")


func _on_menu_music_player_finished():
	await get_tree().create_timer(0.5).timeout
	play("main_menu")
