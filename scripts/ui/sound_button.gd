extends TextureButton

var color = Color("white")
var hover_color = Color("#dadada")

func _on_mouse_entered():
	self.set_modulate(hover_color)
	

func _on_mouse_exited():
	self.set_modulate(color)
