extends CanvasLayer

var menu_color = Color("white")
var menu_hover_color = Color("#dadada")
var menu_click_color = Color("#bababa")

@onready var menu_icon = $MenuIcon

signal pause_menu_called


func _ready():
	hide()
	Globals.game_lost.connect(_on_game_lost)
	Globals.game_started.connect(_on_game_started)
	Globals.game_won.connect(_on_game_won)
	

func _on_game_lost():
	# Don't let the user show the pause menu and end game menu at the same time
	menu_icon.hide()


func _on_game_started():
	show()
	menu_icon.show()


func _on_game_won():
	# Don't let the user show the pause menu and end game menu at the same time
	menu_icon.hide()


func _on_menu_icon_white_mouse_entered():
	menu_icon.set_modulate(menu_hover_color)
	

func _on_menu_icon_white_mouse_exited():
	menu_icon.set_modulate(menu_color)


func _on_menu_icon_gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT:
			if event.pressed:
				menu_icon.set_modulate(menu_click_color)
				pause_menu_called.emit()
			else:
				menu_icon.set_modulate(menu_color)


func _on_pause_menu_game_restarted():
	hide()
	Globals.game_in_progress = false


func _on_end_game_ui_start_menu_called():
	hide()
	Globals.game_in_progress = false
