extends CanvasLayer


func _ready():
	hide()


func _on_white_color_rect_start_color_updated():
	Globals.human_color = "white"
	Globals.bot_color = "black"
	Globals.player_turn = "human"
	

func _on_black_color_rect_start_color_updated():
	Globals.human_color = "black"
	Globals.bot_color = "white"
	Globals.player_turn = "bot"


func _on_start_button_pressed():
	hide()
	Globals.game_started.emit()


func _on_back_to_main_menu_button_pressed():
	hide()
	Globals.main_menu.emit()


func _on_end_game_ui_start_menu_called():
	show()


func _on_main_menu_start_menu_called():
	show()


func _on_pause_menu_start_menu_called():
	show()
