extends Control

signal start_menu_called


func _ready():
	show()
	Globals.main_menu.connect(_on_main_menu)
	

func _on_main_menu():
	show()


func _on_play_button_pressed():
	hide()
	start_menu_called.emit()


func _on_exit_button_pressed():
	get_tree().quit()


func _on_start_game_ui_main_menu_called():
	show()


func _on_end_game_ui_main_menu_called():
	show()


func _on_pause_menu_main_menu_called():
	show()
