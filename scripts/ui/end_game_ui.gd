extends CanvasLayer

signal main_menu_called
signal start_menu_called

@onready var win_lose_label: Label = get_node("EndGameMenu/WinLoseLabel")


func _ready():
	hide()
	
	Globals.game_lost.connect(_on_game_lost)
	Globals.game_started.connect(_on_game_started)
	Globals.game_won.connect(_on_game_won)
	
	
func _on_game_lost():
	win_lose_label.text = "lose"
	show()


func _on_game_started():
	hide()


func _on_game_won():
	win_lose_label.text = "win!"
	show()


func _on_new_game_button_pressed():
	hide()
	start_menu_called.emit()


func _on_quit_game_button_pressed():
	hide()
	Globals.main_menu.emit()
