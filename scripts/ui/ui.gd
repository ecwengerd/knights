extends CanvasLayer

@onready var sound_player = %SoundPlayer


func _ready():
	Globals.game_started.connect(_on_game_started)


func _on_game_started():
	sound_player.play_sound("button")


func _on_state_ui_pause_menu_called():
	if not $PauseMenu.visible:
		sound_player.play_sound("pause")


func _on_main_menu_start_menu_called():
	sound_player.play_sound("button")


func _on_pause_menu_start_menu_called():
	sound_player.play_sound("button")


func _on_end_game_ui_main_menu_called():
	sound_player.play_sound("button")


func _on_end_game_ui_start_menu_called():
	sound_player.play_sound("button")
