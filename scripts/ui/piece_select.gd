extends ColorRect

var OFF_COLOR   := Color("4b4b6d")
var OFF_HOVER_COLOR := Color("535377")
var CLICK_COLOR := Color("8d90cd")
var ON_COLOR    := Color("989bd4")
var ON_HOVER_COLOR := CLICK_COLOR

var mouse_hovering: bool = false
var store_color: Color

signal start_color_updated


func _ready():
	# Default to play as white
	if "White" in self.name:
		self.color = ON_COLOR
	else:
		self.color = OFF_COLOR


# If the player uses the start game window more than once, make sure
# the last color chosen is still preserved and recorded.
func _on_visibility_changed():
	if self.visible and self.color == ON_COLOR:
		start_color_updated.emit()


func _on_mouse_entered():
	mouse_hovering = true
	store_color = self.color
	if store_color == ON_COLOR:
		self.color = ON_HOVER_COLOR
	else:
		self.color = OFF_HOVER_COLOR
	
	
func _on_mouse_exited():
	mouse_hovering = false
	self.color = store_color


# Detect left-clicks
func _on_gui_input(event):
	# InputEventMouseButton has a button_index property - some events
	# do not and will otherwise bug out
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT:
			if event.pressed:
				print("Left button was clicked at ", event.position)
				self.color = CLICK_COLOR
			# If left mouse button is released while mouse is hovered
			elif not event.pressed and mouse_hovering:
				start_color_updated.emit()
				self.color = ON_COLOR
				store_color = self.color
	
	elif event is InputEventScreenTouch:
		if event.pressed:
			self.color = CLICK_COLOR
		elif not event.pressed:
			start_color_updated.emit()
			self.color = ON_COLOR
			store_color = self.color


func _on_white_color_rect_start_color_updated():
	if "Black" in self.name:
		self.color = OFF_COLOR
	else:
		self.color = ON_COLOR


func _on_black_color_rect_start_color_updated():
	if "White" in self.name:
		self.color = OFF_COLOR
	else:
		self.color = ON_COLOR
