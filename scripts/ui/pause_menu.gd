extends CanvasLayer

signal start_menu_called
signal game_restarted


func _ready():
	hide()
	
	Globals.game_lost.connect(_on_game_lost)
	Globals.game_won.connect(_on_game_won)


# Auto-hide the pause menu if the game ends while paused
func _on_game_lost():
	hide()
	
func _on_game_won():
	hide()
	

func _on_resume_button_pressed():
	hide()


func _on_restart_button_pressed():
	hide()
	start_menu_called.emit()
	# Add a game restart signal so the state UI doesn't linger
	game_restarted.emit()


func _on_quit_game_button_pressed():
	hide()
	# Add a game restart signal so the state UI doesn't linger
	game_restarted.emit()
	Globals.main_menu.emit()


func _on_state_ui_pause_menu_called():
	if not self.visible:
		show()
	else:
		hide()
