extends GridContainer

var flipped := false

@export var debug_mode: bool = false

@onready var white_knight: PackedScene = preload("res://scenes/pieces/white_knight.tscn")
@onready var black_knight: PackedScene = preload("res://scenes/pieces/black_knight.tscn")


func _ready():
	# Get the screen size and screen center.
	var resolution = get_viewport_rect().size
	print(resolution)


func flip():
	self.rotation_degrees += 180


func clear():
	for square in self.get_children():
		for piece in square.get_children():
			piece.queue_free()


func _animate_placement(
	piece: Piece,
	square: Square,
	duration: float,
	transition: Tween.TransitionType = Tween.TRANS_EXPO,
):
	
	var animate_from = Vector2(
		square.get_global_xform_position().x,
		get_viewport_rect().size.y * -1,  # Off-screen, above the board
	)
	var animate_to = square.get_global_xform_position()
	await piece.animate(
		animate_from,
		animate_to,
		transition,
		duration,
	)


func _place_piece(piece_scene: PackedScene, square: Square):
	var piece: Piece = piece_scene.instantiate()
	
	var piece_color = "white" if "white" in piece_scene.resource_path else "black"
	piece.add_to_group(piece_color)
	piece.add_to_group("pieces")
	
	# Piece textures may rotate with the board and appear
	# upside-down if not rotated separately.
	if self.flipped:
		piece.rotation_degrees += 180
		
	square.add_child(piece)
	
	const animation_duration = 1.0  # seconds
	await _animate_placement(piece, square, animation_duration)
	
	piece.set_global_position(square.get_global_xform_position())


func setup(
	white_squares: Array = ["a1", "c1", "f1", "h1", "b2", "d2", "e2", "g2", "a3", "c3", "f3", "h3"],
	black_squares: Array = ["a6", "c6", "f6", "h6", "b7", "d7", "e7", "g7", "a8", "c8", "f8", "h8"],
):
	
	if debug_mode == true:
		white_squares = ["e2"]
		black_squares = ["d4"]
	
	clear()
	
	# Rotate the board so the chosen color faces the player.
	if Globals.human_color == "black" and not flipped:
		flip()
		flipped = true
	elif Globals.human_color == "white" and flipped:
		flip()
		flipped = false
		
	var number_of_pieces = len(white_squares) + len(black_squares)
	var total_wait_time = 0.4
	var wait_time = total_wait_time / number_of_pieces / 2  # Wait time per piece, per color
	
	for square in white_squares:
		square = self.find_child(square)
		await get_tree().create_timer(wait_time).timeout
		_place_piece(white_knight, square)
	for square in black_squares:
		square = self.find_child(square)
		await get_tree().create_timer(wait_time).timeout
		_place_piece(black_knight, square)
