extends Node2D

var is_game_paused: bool = false
var winning_color = null


func _ready():
	Globals.game_started.connect(_on_game_started)
	Globals.turn_ended.connect(_on_turn_ended)


func _is_knight(piece: Piece) -> bool:
	return piece is Knight


func _is_queen(piece: Piece) -> bool:
	return piece is Queen
	

func _on_game_started() -> void:
	Globals.color_turn = "white"  # White goes first, unless today is opposite day
	if Globals.human_color == "white":
		Globals.player_turn = "human"
	else:
		Globals.player_turn = "bot"
	
	# Give extra time for the beginning animations to finish completely before
	# the next player can move.
	await get_tree().create_timer(0.8).timeout
	Globals.game_in_progress = true
	winning_color = null


func _on_turn_ended(piece: Piece) -> void:
	
	# Check for win conditions. The player wins if:
	# 1) If all opponent pieces are eliminated, OR
	# 2) All opponent knights are eliminated, AND
	#    the player has more queens than the opponent.
	for color in ["white", "black"]:
		var pieces: Array = get_tree().get_nodes_in_group(color)
		var opposite_color = "black" if color == "white" else "white"
		
		# Check win condition #1
		if pieces.is_empty():
			Globals.game_in_progress = false
			winning_color = opposite_color
		
		# Check win condition #2
		else:
			if not pieces.any(_is_knight):
				var opposite_color_pieces = get_tree().get_nodes_in_group(opposite_color)
				
				var color_queens = pieces.map(_is_queen).count(true)
				var opposite_color_queens = opposite_color_pieces.map(_is_queen).count(true)
				
				if color_queens < opposite_color_queens:
					Globals.game_in_progress = false
					winning_color = opposite_color
	
	# If no win conditions are met, update turn order. Otherwise,
	# end the game
	if Globals.game_in_progress:
		Globals.color_turn = piece.opposite_color
		Globals.player_turn = "bot" if Globals.player_turn == "human" else "human"
	else:
		if winning_color == Globals.bot_color:
			Globals.game_lost.emit()
		elif winning_color == Globals.human_color:
			Globals.game_won.emit()
