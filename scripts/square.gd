class_name Square extends ColorRect

var is_light_square: bool

@onready var board = self.get_parent()
@onready var coords = Globals.convert_notation_to_coordinates(self.name)

const dark_square_color: Color     = Color("#39bf68")
const light_square_color: Color    = Color("#83ffa6")
const dark_highlight_color: Color  = Color("#4ed37c")
const light_highlight_color: Color = Color("#a4fcbd")


func _ready():
	# Check if the square is light or dark.
	if (coords.x + coords.y) % 2 == 0:
		is_light_square = false
	else:
		is_light_square = true


func get_global_xform_position() -> Vector2:
	"""Get a global position vector that remains constant, even if the square is
	rotated. See https://github.com/godotengine/godot/issues/95798, first animation, 
	bottom-right corner for actual behavior of global_position when rotated."""
	
	# Equals 0 if no rotation, and equals self.size if rotated 180 degrees
	var transform_offset = (self.size / 2) - self.get_global_transform().basis_xform(self.size / 2)
	
	return self.global_position - transform_offset


func highlight() -> void:
	if is_light_square:
		self.color = light_highlight_color
	else:
		self.color = dark_highlight_color
	
	
func unhighlight() -> void:
	if is_light_square:
		self.color = light_square_color
	else:
		self.color = dark_square_color


# Detect left-clicks
func _on_gui_input(event):
	# InputEventMouseButton has a button_index property - some events
	# do not and will otherwise bug out
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT:
			if event.pressed:
				print("Left button was pressed at ", event.position)
				Globals.from_square = self
				Globals.square_selected.emit("pressed")
			else:  # Released
				print("Left button was released at ", event.position)
				Globals.square_selected.emit("released")

	elif event is InputEventScreenTouch:
		if event.pressed:
			Globals.from_square = self
			Globals.square_selected.emit("pressed")
		else:
			Globals.square_selected.emit("released")


func _on_mouse_entered():
	Globals.to_square = self
	print(Globals.to_square)
