extends AudioStreamPlayer

var muted = false

var sound_bank: Dictionary = {
	"button": preload("res://sounds/click_button.mp3"),
	"pause": preload("res://sounds/click_pause_menu.mp3"),
	"move": preload("res://sounds/move_piece.wav"),
	"capture": preload("res://sounds/capture_piece.wav"),
	"promote": preload("res://sounds/promote_piece.wav"),
	"win": preload("res://sounds/win_game.wav"),
	"lose": preload("res://sounds/lose_game.wav"),
	"restart": preload("res://sounds/restart_game.wav"),
}


func play_sound(action):
	
	self.stream = sound_bank[action]
	if not muted:
		self.play()


func _on_sound_button_toggled(toggled_on):
	muted = false if toggled_on else true
