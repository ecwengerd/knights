extends Node2D

@export var board_manager: Node2D

var bot_turn_complete: bool = true


func _get_all_pieces(color):
	
	return get_tree().get_nodes_in_group(color)
	

func _process(_delta):
	
	# Without a "turn complete" variable to check concurrency,
	# _process() will continue to loop for the duration of the
	# bot's turn, which may result in multiple moves at once.
	# Consider replacing this with a signal.
	if bot_turn_complete:
		if Globals.player_turn == "bot" and Globals.game_in_progress == true:
			bot_turn_complete = false
			# Add a semi-random wait between turns so that the bot
			# doesn't immediately move after the human player has moved.
			var wait_time = randf_range(1.0, 1.5)
			await get_tree().create_timer(wait_time).timeout
			await _calculate_next_move()


func _get_all_opponent_available_squares():
	# This function may return duplicate values of available
	# squares.
	
	var opponent_available_squares = []
	var opponent_pieces = get_tree().get_nodes_in_group(Globals.human_color)
	for piece in opponent_pieces:
		var squares = board_manager.get_available_squares(piece)
		opponent_available_squares += squares
		
	return opponent_available_squares


func _find_promotions(pieces):
	# Finds all available promotion moves.
	# Returns {piece, [to_square, to_square, ...], ...}
	
	var available_promotions = {}
	for piece in pieces:
		if piece is Knight:  # Can't promote a queen
			var available_squares = board_manager.get_available_squares(piece)
			for to_square in available_squares:
				if (
					to_square.coords.y == 1 and Globals.bot_color == "black" or
					to_square.coords.y == 8 and Globals.bot_color == "white"
				) and not (
					# Could immediately be captured
					to_square in _get_all_opponent_available_squares()
				):
					if piece in available_promotions:
						available_promotions[piece].append(to_square)
					else:
						available_promotions[piece] = [to_square]
	return available_promotions


func _find_captures(pieces, piece_type):

	var captures = {}
	for piece in pieces:
		var available_squares = board_manager.get_available_squares(piece)
		for to_square in available_squares:
			var piece_on_move_square = board_manager.get_piece_on_square(to_square)
			if piece_on_move_square != null:
				if not piece_on_move_square.is_in_group(Globals.bot_color):
					if is_instance_of(piece_on_move_square, piece_type):
						if piece in captures:
							captures[piece].append(to_square)
						else:
							captures[piece] = [to_square]
	return captures


func _try_to_promote(pieces):
	# Checks if there are any possible promotion moves
	# then chooses one at random if so

	var available_promotions = _find_promotions(pieces)
	if available_promotions:
		print("Found possible promotions:")
		print(available_promotions)
		var piece = available_promotions.keys().pick_random()
		await board_manager.play_move(piece, available_promotions[piece].pick_random())
		return true
	return false


func _try_to_capture(pieces):
		
	var captures = {  # In order of most to least important
		Queen: {},
		Knight: {}
	}
	for piece_type in captures.keys():
		captures[piece_type] = _find_captures(pieces, piece_type)
		var piece
		var possible_squares
		if captures[piece_type]:
			print("Found possible ", piece_type, "s to capture:")
			print(captures[piece_type])
			piece = captures[piece_type].keys().pick_random()
			possible_squares = captures[piece_type][piece]
		
		if piece and possible_squares:
			await board_manager.play_move(piece, possible_squares.pick_random())
			return true
	return false


func _move_wherever_you_want_i_dont_care(pieces):
	
	var piece = pieces.pick_random()
	var available_squares = board_manager.get_available_squares(piece)
	var to_square = available_squares.pick_random()
	
	await board_manager.play_move(piece, to_square)


func _calculate_next_move():
	
	var pieces: Array = _get_all_pieces(Globals.bot_color)
	pieces.shuffle()
	
	bot_turn_complete = await _try_to_promote(pieces)
	if not bot_turn_complete:
		bot_turn_complete = await _try_to_capture(pieces)
	if not bot_turn_complete:
		await _move_wherever_you_want_i_dont_care(pieces)
	bot_turn_complete = true
