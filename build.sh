set -xu

export TEMPLATES_DIR=".local/share/godot/export_templates/${GODOT_VERSION}.stable"

mkdir binaries
wget --no-verbose https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-stable/Godot_v${GODOT_VERSION}-stable_linux.x86_64.zip
unzip Godot_v${GODOT_VERSION}-stable_linux.x86_64.zip
mv Godot_v${GODOT_VERSION}-stable_linux.x86_64 /usr/bin/godot


mkdir -p ~/${TEMPLATES_DIR}
wget --no-verbose https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-stable/Godot_v${GODOT_VERSION}-stable_export_templates.tpz -O templates.zip
unzip -j templates.zip -d ~/${TEMPLATES_DIR}/

godot --version
godot --headless --export-release "Windows Desktop" binaries/knights.exe
godot --headless --export-release "macOS" binaries/knights.zip

#############################################
# Android - not working on my Pixel 7
#############################################

# API_URL="https://api.adoptium.net/v3/binary/latest/${JDK_VERSION}/ga/linux/x64/jdk/hotspot/normal/eclipse"

# FETCH_URL=$(curl -s -w %{redirect_url} "${API_URL}")
# JDK_FILENAME=$(curl -OLs -w %{filename_effective} "${FETCH_URL}")

# # Validate the checksum
# curl -Ls "${FETCH_URL}.sha256.txt" | sha256sum -c --status

# mkdir ${JAVA_HOME}
# tar xzvf ${JDK_FILENAME} --directory ${JAVA_HOME} --strip-components=1

# mkdir ${ANDROID_HOME}
# wget --no-verbose https://dl.google.com/android/repository/commandlinetools-linux-11076708_latest.zip -O ${ANDROID_HOME}/sdkcmd.zip
# unzip ${ANDROID_HOME}/sdkcmd.zip -d ${ANDROID_HOME}
# mkdir ${ANDROID_HOME}/cmdline-tools/latest
# mv ${ANDROID_HOME}/cmdline-tools/bin ${ANDROID_HOME}/cmdline-tools/latest/
# mv ${ANDROID_HOME}/cmdline-tools/lib ${ANDROID_HOME}/cmdline-tools/latest/
# mv ${ANDROID_HOME}/cmdline-tools/source.properties ${ANDROID_HOME}/cmdline-tools/latest/

# yes | ${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager --licenses
# ${ANDROID_HOME}/cmdline-tools/latest/bin/sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" "build-tools;34.0.0" "platforms;android-34" "cmdline-tools;latest" "cmake;3.10.2.4988404" "ndk;23.2.8568313"

# godot --headless --export-release "Android" binaries/knights_android.apk